#include <criterion/criterion.h>
#include "my_add.h"

Test(my_add, zero_plus_zero)
{
    // GIVEN
    int x = 0;
    int y = 0;

    // WHEN
    int result = my_add(x, y);

    // THEN
    cr_assert_eq(result, 0);
}

Test(my_add, two_positive_numbers)
{
    // GIVEN
    int x = 42;
    int y = 51;

    // WHEN
    int result = my_add(x, y);

    // THEN
    cr_assert_eq(result, 93);
}

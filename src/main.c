#include <stdio.h>
#include <stdlib.h>

#include "my_add.h"

int main(int argc, char *argv[])
{
    if (argc != 3)
        return 1;

    printf("%d\n", my_add(atoi(argv[1]), atoi(argv[2])));
    return 0;
}

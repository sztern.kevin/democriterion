FLAGS = -Wall -Wextra -Werror -std=c99 -pedantic
CPPFLAGS = -Isrc/

OBJS = $(addprefix src/, my_add.o)

TEST_OBJS = $(addprefix tests/, tests_my_add.o)
TEST_LDLIBS = -lcriterion

all: my_add

my_add: src/main.o $(OBJS)
	$(CC) -o $@ $(LDLIBS) $^

testsuite: $(OBJS) $(TEST_OBJS)
	$(CC) -o $@ $(TEST_LDLIBS) $^

test: testsuite
	./testsuite

clean:
	rm -f $(OBJS) $(TEST_OBJS) src/main.o my_add testsuite

.PHONY: test all clean
